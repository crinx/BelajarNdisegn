# Introduction to Colors

## RGB Colors
RGB is red green blue color value, each parameter of color as an integers between 0 and 255
For example, rgb(255,0,0) is red

## Hexadecimal Colors
Hexadecimal colors is RR (red), GG (green) and BB (blue) value, each parameter of color as an integers between 00 and FF
For example, #FF0000 is red
